cmake_minimum_required(VERSION 3.7)
add_compile_options(-std=c++17)
project(vulkan_gpu_fourier)

set (VKFFT_BACKEND 0 CACHE STRING "0-Vulkan")

set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT ${PROJECT_NAME})
add_definitions(-D_CRT_SECURE_NO_WARNINGS)
add_executable(${PROJECT_NAME} src/Vulkan_FFT.cpp src/utils_VkFFT.cpp src/R2C_C2R.cpp)

target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include/includeDir/)

find_package(Vulkan REQUIRED)
target_compile_definitions(${PROJECT_NAME} PUBLIC -DVK_API_VERSION=11) #10 - Vulkan 1.0, #11 - Vulkan 1.1, #12 - Vulkan 1.2

target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/stories/glslang/glslang/Include/)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/stories/glslang)

target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include/vkFFT/)
add_library(VkFFT INTERFACE)
target_compile_definitions(VkFFT INTERFACE -DVKFFT_BACKEND=${VKFFT_BACKEND})

target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/stories/half_lib/)
add_library(half INTERFACE)

target_link_libraries(${PROJECT_NAME} PUBLIC SPIRV glslang Vulkan::Vulkan VkFFT half)

configure_file(stories/vulkangpu_fourier_config.ini vulkangpu_fourier_config.ini COPYONLY)

enable_testing()
add_test(Launch vulkan_gpu_fourier -a)


