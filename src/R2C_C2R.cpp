/** \author Пинегина Н.А.
  * \file R2C_C2R.cpp
  * \brief Определение функций необходимых для генерации последовательности заданной длины, построения "плана", выполннения необходимых преобразований.*/

#include <stdio.h>
#include <vector>
#include <memory>
#include <string.h>
#include <chrono>
#include <thread>
#include <iostream>
#ifndef __STDC_FORMAT_MACROS
#define __STDC_FORMAT_MACROS
#endif
#include <inttypes.h>
#include "fftw3.h"

using namespace std;
using namespace chrono;

#include "vulkan/vulkan.h"
#include "glslang_c_interface.h"
#include "vkFFT.h"
#include "utils_VkFFT.h"
#include <ctime>
#include <iomanip>

#define MODULE_SAYS "[vulkan_gpu_fourier]:"

using namespace std;

// Генерация сигнала.
double SignalR(int t)
{
	double result = 0;
	double Amplitude[] = { 4, 2, 7 };
	double Sigma[] = { 10.,5.,7. };
	double Phase[] = { 6.28,3.14,0. };
	double W[] = { 0.05, 0.01, 0.08 };
	double Centre_domes[] = { 100.,230.,280. };
	for (int i = 0; i < 3; i++)
	{
		//result += Amplitude[i] * exp(-((t - Centre_domes[i]) / Sigma[i]) * ((t - Centre_domes[i]) / Sigma[i]));
		result += Amplitude[i] * sin(W[i] * t + Phase[i]);
		//result += sin(t);
	}
	return result;
}

// Генерация шума.
int CreateShum(std::vector<float>& Signal, std::vector<float>& Shum)
{
	double r[13] = { 0 }, t1 = 0, t2 = 0, Kol, Summ_Eps = 0;
	vector<float> Eps;
	Eps.resize(Signal.size());
	Shum.resize(Signal.size());
	for (int j = 0; j < Signal.size(); j++)
	{
		Kol = 0;
		for (int i = 0; i < 13; i++)
		{
			r[i] = 2 * (rand() % 100) / (100 * 1.0) - 1;
			Kol += r[i];
		}
		Eps[j] = Kol / 12;
		Summ_Eps += Eps[j] * Eps[j];
	}
	double Es = 0, En = 0;
	double n = 0;
	double energy = n / 100;
	double result = 0;
	for (int i = 0; i < Signal.size(); i++)
	{
		Es += Signal[i] * Signal[i];
	}
	//энергия шума
	En = energy * Es;
	//вычисление коэфициента 
	double alpha = sqrt(En / Summ_Eps);
	//отрисовка шума, заполнение массива сигнал+шум
	for (int j = 0; j < Signal.size(); j++)
	{
		Shum[j] = Eps[j] * alpha;
	}
	return 0;
}
/** Печать метки локального времени на экран.
	@param forMsgErr печать метки в поток сообщений об ошибках (cerr).*/
void printtimestamp(bool forMsgErr = false)
{
	auto now = chrono::system_clock::now();
	// Получение микросекунд для текущей секунды.
	auto us = chrono::duration_cast<chrono::microseconds>(now.time_since_epoch()) % 1000000;
	// Преобразование в другой формат с целью последующей локализации.
	time_t timer = chrono::system_clock::to_time_t(now);
	// Получение локального времени.
	tm lt = *localtime(&timer);

	// Формирование и печать строки с датой и временем. Формат строки: " YYYY-mm-dd HH:MM:SS.µs ".
	if (forMsgErr)
	{
		cerr << ' ' << put_time(&lt, "%F %T") << '.' << setfill('0') << setw(6) << us.count() << ' ';

	}
	else
	{
		cout << ' ' << put_time(&lt, "%F %T") << '.' << setfill('0') << setw(6) << us.count() << ' ';
	}

}

/** Сформировать последовательность заданной длины, построить "план", выполнить передачу данных с хоста на GPU, выполнить преобразование Фурье по постороенному "плану", передать данные с GPU на хост.
	 @param vkGPU - структура, содержащая примитивы Vulkan.
	 @param isCompilerInitialized.
	 @param arrLgth - длина массива.
	 @param loads - количество загрузок.
	 @param timeStart метка - начала времени.
	 @return VKFFT_SUCCESS - в случае успеха, код ошибки в противном случае.*/
VkFFTResult R2C_C2R(VkGPU* vkGPU,  uint64_t isCompilerInitialized, bool file_output, FILE* output, uint64_t arrLgth, int loads, high_resolution_clock::time_point timeStart)
{
	VkFFTResult resFFT = VKFFT_SUCCESS;
	VkResult res = VK_SUCCESS;

    uint64_t benchmark_dimensions[1][4]={{(uint64_t)arrLgth, 1, 1, 1}};

	for (uint64_t z = 0; z < loads; z++)
	{
		int n = 0;
		uint64_t dims[3] = { benchmark_dimensions[n][0] , benchmark_dimensions[n][1] , benchmark_dimensions[n][2]};
		vector<float> in;
		for (uint64_t i = 0; i < dims[0] * dims[1] * dims[2]; i++)
		{
			//in.push_back(SignalR(i));
			in.push_back((float)(2 * ((float)rand()) / RAND_MAX - 1.0));
		}
		vector<float> Shum;
		CreateShum(in, Shum);
		for (int i = 0; i < in.size(); i++)
		{
			in[i] = in[i] + Shum[i];
		}

		uint64_t t = 0;
		float* inputC = (float*)(malloc(sizeof(float) * (dims[0]) * dims[1] * dims[2]));
		for (uint64_t l = 0; l < dims[2]; l++) {
			for (uint64_t j = 0; j < dims[1]; j++) {
				for (uint64_t i = 0; i < dims[0]; i++) 
				{
					inputC[i + j * (dims[0]) + l * (dims[0]) * dims[1]] = in[t];
					t++;
				}
			}
		}
		int num_iter = 1;

		VkFFTConfiguration configuration = {};
		VkFFTApplication app = {};
		// Размер БПФ, 1D, 2D или 3D (по умолчанию 1).
		configuration.FFTdim = benchmark_dimensions[n][3]; 
		// Размеры многомерных измерений БПФ(по умолчанию 1).
		configuration.size[0] = benchmark_dimensions[n][0];  
		configuration.size[1] = benchmark_dimensions[n][1];
		configuration.size[2] = benchmark_dimensions[n][2];
		// Выполнить разложение R2C/C2R (0 - выкл., 1 - вкл.)
		configuration.performR2C = 1;
		// возвращает данные во входной буфер в обратном преобразовании (0 - выключено, 1 - включено).
		configuration.inverseReturnToInputBuffer = 1;
		configuration.device = &vkGPU->device;
		configuration.queue = &vkGPU->queue;
		configuration.fence = &vkGPU->fence;
		configuration.commandPool = &vkGPU->commandPool;
		configuration.physicalDevice = &vkGPU->physicalDevice; 
		configuration.isCompilerInitialized = isCompilerInitialized;													

		uint64_t numBuf = 1;

		uint64_t* inputBufferSize = (uint64_t*)malloc(sizeof(uint64_t) * numBuf);
		if (!inputBufferSize) return VKFFT_ERROR_MALLOC_FAILED;
		for (uint64_t i = 0; i < numBuf; i++) 
		{
			inputBufferSize[i] = {};
			inputBufferSize[i] = (uint64_t)sizeof(float) * configuration.size[0] * configuration.size[1] * configuration.size[2] / numBuf;
		}
		uint64_t* bufferSize = (uint64_t*)malloc(sizeof(uint64_t) * numBuf);
		if (!bufferSize) return VKFFT_ERROR_MALLOC_FAILED;
		for (uint64_t i = 0; i < numBuf; i++) 
		{
			bufferSize[i] = {};
			bufferSize[i] = (uint64_t)sizeof(float) * 2 * (configuration.size[0] / 2 + 1) * configuration.size[1] * configuration.size[2] / numBuf;
		}

		VkBuffer* ibuffer = (VkBuffer*)malloc(numBuf * sizeof(VkBuffer));
		if (!ibuffer) return VKFFT_ERROR_MALLOC_FAILED;
		VkDeviceMemory* ibufferDeviceMemory = (VkDeviceMemory*)malloc(numBuf * sizeof(VkDeviceMemory));
		if (!ibufferDeviceMemory) return VKFFT_ERROR_MALLOC_FAILED;
		VkBuffer* buffer = (VkBuffer*)malloc(numBuf * sizeof(VkBuffer));
		if (!buffer) return VKFFT_ERROR_MALLOC_FAILED;
		VkDeviceMemory* bufferDeviceMemory = (VkDeviceMemory*)malloc(numBuf * sizeof(VkDeviceMemory));
		if (!bufferDeviceMemory) return VKFFT_ERROR_MALLOC_FAILED;

		for (uint64_t i = 0; i < numBuf; i++)
		{
			buffer[i] = {};
			bufferDeviceMemory[i] = {};
			resFFT = allocateBuffer(vkGPU, &buffer[i], &bufferDeviceMemory[i], VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | 
				VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_HEAP_DEVICE_LOCAL_BIT, bufferSize[i]);
			if (resFFT != VKFFT_SUCCESS) return resFFT;
			ibuffer[i] = {};
			ibufferDeviceMemory[i] = {};
			resFFT = allocateBuffer(vkGPU, &ibuffer[i], &ibufferDeviceMemory[i], VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | 
				VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_HEAP_DEVICE_LOCAL_BIT, inputBufferSize[i]);
			if (resFFT != VKFFT_SUCCESS) return resFFT;
		}

		configuration.inputBufferNum = numBuf;
		configuration.bufferNum = numBuf;
		configuration.bufferSize = bufferSize;
		configuration.isInputFormatted = 1;
		configuration.inputBufferStride[0] = configuration.size[0];
		configuration.inputBufferStride[1] = configuration.size[0] * configuration.size[1];
		configuration.inputBufferStride[2] = configuration.size[0] * configuration.size[1] * configuration.size[2];

		configuration.inputBufferSize = inputBufferSize;

		if (z == 0)
		{
			std::chrono::high_resolution_clock::time_point timeFinish = chrono::high_resolution_clock::now();
			long timeDur = chrono::duration_cast<chrono::nanoseconds>(timeFinish - timeStart).count();
			cout << MODULE_SAYS;
			printTimestamp(false);
			cout<<"Time to generate a GPU plan: " <<  timeDur << " ns." << endl;
			timeStart = timeFinish;
			if (file_output)
				fprintf(output, "Time to generate a GPU plan: %d \n", timeDur);
		}
			
		uint64_t shift = 0;
		
		for (uint64_t i = 0; i < numBuf; i++)
		{
			resFFT = transferDataFromCPU(vkGPU, (inputC /*+ shift / sizeof(fftwf_complex)*/), &ibuffer[i], inputBufferSize[i]);
			if (resFFT != VKFFT_SUCCESS) return resFFT;
			shift += inputBufferSize[i];
		}
 
		resFFT = initializeVkFFT(&app, configuration);
		if (resFFT != VKFFT_SUCCESS) return resFFT;

		VkFFTLaunchParams launchParams = {};
		launchParams.inputBuffer = ibuffer;
		launchParams.buffer = buffer;

		/*double totTime = 0;
		resFFT = performVulkanFFT(vkGPU, &app, &launchParams, -1, num_iter);
		if (resFFT != VKFFT_SUCCESS) return resFFT;*/

		double time = 0;
		resFFT = performVulkanFFTiFFT(vkGPU, &app, &launchParams, num_iter, &time);
		if (resFFT != VKFFT_SUCCESS) return resFFT;
		

		float* output_VkFFT = (float*)(malloc(bufferSize[0]));
		if (!output_VkFFT) return VKFFT_ERROR_MALLOC_FAILED;

		shift = 0;
		for (uint64_t i = 0; i < numBuf; i++)
		{
			resFFT = transferDataToCPU(vkGPU, (output_VkFFT /*+ shift / sizeof(fftwf_complex)*/), &ibuffer[i], inputBufferSize[i]);
			if (resFFT != VKFFT_SUCCESS) return resFFT;
			shift += inputBufferSize[i];
		}


		
		free(output_VkFFT);
		
		for (uint64_t i = 0; i < numBuf; i++) 
		{

			vkDestroyBuffer(vkGPU->device, buffer[i], NULL);
			vkFreeMemory(vkGPU->device, bufferDeviceMemory[i], NULL);
			vkDestroyBuffer(vkGPU->device, ibuffer[i], NULL);
			vkFreeMemory(vkGPU->device, ibufferDeviceMemory[i], NULL);
		}

		free(buffer);
		free(bufferDeviceMemory);
		free(ibuffer);
		free(ibufferDeviceMemory);

		free(inputBufferSize);
		free(bufferSize);
		deleteVkFFT(&app);
		free(inputC);
		if (z % 10 == 0)
		{ 
			int znah_osh = (int)resFFT;
			if (znah_osh==0)
			{ 
				cout << MODULE_SAYS;
				printTimestamp(false);
				cout << "Current download number:" << z << "\t" << "The function returned: VKFFT_SUCCESS " << endl;
				if (file_output)
				{
					fprintf(output, "Current download number: %d \t The function returned: VKFFT_SUCCESS \n", z);
				}
			}
			else
			{ 
				cout << MODULE_SAYS;
				printTimestamp(false);
				cout << "Current download number:" << z << "\t" << "The function returned: " << resFFT << endl;
				if (file_output)
				{
					fprintf(output, "Current download number: %d \t The function returned: %d \n", z, resFFT);
				}
			}
		}
	}
	return resFFT;
}