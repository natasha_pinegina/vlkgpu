﻿/** \author Пинегина Н.А.
  * \file Vulkan_FFT.cpp
  * \brief Реализация программы, обеспечивающей нагрузкой GPU вычислительного узла.*/

#include <vector>
#include <memory>
#include <string>
#include <chrono>
#include <thread>
#include <iostream>
#include <algorithm>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include "vulkan/vulkan.h"
#include "glslang_c_interface.h"

#include "vkFFT.h"
#include "R2C_C2R.h"
#include "utils_VkFFT.h"
#include "half.hpp"
#include "user_benchmark_VkFFT.h"
#include <fstream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <iomanip>
//#include <Time.h>
#define MODULE_SAYS "[vulkan_gpu_fourier]:"

using namespace std;
using namespace chrono;

 /** Инициализировать.
	 @param vkGPU - структура, содержащая примитивы Vulkan.
	 @param sample_id .
	 @param file_output - логическая переменная показывает, будет ли производиться запись в файл.
	 @param output - .
	 @param userParams структура, использующаяся для передачи определяемых пользователем параметров. 
	 @param arrLgth длина массива.
	 @param loads количество загрузок.
	 @param timeStart метка начала времени.
	 @return VKFFT_SUCCESS - в случае успеха, код ошибки в противном случае.*/
VkFFTResult launchVkFFT(VkGPU* vkGPU, uint64_t sample_id, bool file_output, FILE* output, VkFFTUserSystemParameters* userParams, uint64_t arrLgth,int loads, std::chrono::high_resolution_clock::time_point timeStart)
{
	VkFFTResult resFFT = VKFFT_SUCCESS;
	VkResult res = VK_SUCCESS;

	// Создать экземпляр vulkan с конкретными параметрами приложения.
	res = createInstance(vkGPU, sample_id);
	if (res != 0)
	{
		return VKFFT_ERROR_FAILED_TO_CREATE_INSTANCE;
	}

	// Настроить отладочный мессенджер.
	res = setupDebugMessenger(vkGPU);
	if (res != 0)
	{
		return VKFFT_ERROR_FAILED_TO_SETUP_DEBUG_MESSENGER;
	}

	// Найти физическое устройство (графического процессора), поддерживающего Vulkan.
	res = findPhysicalDevice(vkGPU);
	if (res != 0) 
	{
		return VKFFT_ERROR_FAILED_TO_FIND_PHYSICAL_DEVICE;
	}

	VkPhysicalDeviceProperties deviceProperties;
	vkGetPhysicalDeviceProperties(vkGPU->physicalDevice, &deviceProperties);

	// Создать логическое устройство.
	res = createDevice(vkGPU, sample_id);
	if (res != 0) 
	{
		return VKFFT_ERROR_FAILED_TO_CREATE_DEVICE;
	}
	// Создать ограждение для синхронизации. 
	res = createFence(vkGPU);
	if (res != 0) 
	{
		return VKFFT_ERROR_FAILED_TO_CREATE_FENCE;
	}
	// Создать место, из которого выделяется память буфера команд.
	res = createCommandPool(vkGPU);
	if (res != 0) 
	{
		return VKFFT_ERROR_FAILED_TO_CREATE_COMMAND_POOL;
	}

	// Вернуть свойства физического устройства.
	vkGetPhysicalDeviceProperties(vkGPU->physicalDevice, &vkGPU->physicalDeviceProperties);
	// Сообщить информацию о памяти для указанного физического устройства.
	vkGetPhysicalDeviceMemoryProperties(vkGPU->physicalDevice, &vkGPU->physicalDeviceMemoryProperties);

	// Инициализировать компилятор.
	glslang_initialize_process();

	uint64_t isCompilerInitialized = 1;

	high_resolution_clock::time_point timeFinish = chrono::high_resolution_clock::now();
	long timeDur = chrono::duration_cast<chrono::nanoseconds>(timeFinish - timeStart).count();
	cout << MODULE_SAYS;
	printTimestamp(false);
	cout << "GPU preparation time: " << timeDur << " ns." << endl;
	if (file_output)
		fprintf(output, "GPU preparation time: %d \n", timeDur);
	// Выполнить генерацию последовательности заданной длины, построить "план" для выполнения перобразования Фурье, выполнить необходимые преобразования.
	resFFT = R2C_C2R(vkGPU, isCompilerInitialized, file_output, output, arrLgth,loads, timeFinish);

	// Уничтожить объект ограждения.
	vkDestroyFence(vkGPU->device, vkGPU->fence, NULL);
	// Уничтожить объект пула команд.
	vkDestroyCommandPool(vkGPU->device, vkGPU->commandPool, NULL);
	// Уничтожить логическое устройство. 
	vkDestroyDevice(vkGPU->device, NULL);
	//
	DestroyDebugUtilsMessengerEXT(vkGPU, NULL);
	// Уничтожить экземплар Vulkan.
	vkDestroyInstance(vkGPU->instance, NULL);
	// Уничтожить компилятор.
	glslang_finalize_process();

	return resFFT;
}

int main(int argc, char* argv[])
{
	//setlocale(LC_ALL, "Russian");
	
	if (argc > 1) 
	{
		auto arg = string(argv[1]);
		if (arg == "-v" || arg == "--version") 
		{
			    cout << MODULE_SAYS;
			    printTimestamp(false);
			    return 0;
			
		}
		showErr("Unknown program argument");
	}

	auto configs = readConfigFile();
	const uint64_t arrLgth = configs[0];
	const double loads = configs[1];

	bool file_output = true;
	FILE* output = NULL;
	output = fopen("result.txt", "w");

	if (arrLgth !=0 && loads !=0 && file_output)
		fprintf(output, "The configuration file has been read\n");

	high_resolution_clock::time_point timeStart = chrono::high_resolution_clock::now();
	
	VkGPU vkGPU = {};
	vkGPU.enableValidationLayers = 0;

	uint64_t sample_id = 0;
	
	launchVkFFT(&vkGPU, sample_id, file_output, output, 0, arrLgth,loads, timeStart);
	
	high_resolution_clock::time_point timeFinish = chrono::high_resolution_clock::now();
	long timeDur = chrono::duration_cast<chrono::nanoseconds>(timeFinish - timeStart).count();
	long timeAvg = static_cast <long> (timeDur / static_cast <double> (loads));

	cout << MODULE_SAYS;
	printTimestamp(false);
	cout << "Total time spent on the Fourier transform: " << abs(timeDur) << endl;
	showAvgTime(abs(timeAvg));

	if (file_output)
	{
		fprintf(output, "Total time spent on the Fourier transform: %d\n", abs(timeDur));
		fprintf(output, "Average time spent on one Fourier transform: %d\n", abs(timeAvg));
	}

	fclose(output);
	return 0;
}
