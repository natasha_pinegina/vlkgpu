/** \author �������� �.�.
  * \file R2C_C2R.h
  * \brief ���������� ������� ����������� ��� ��������� ������������������ �������� �����, ���������� "�����" ��� ���������� �������������� �����, ����������� ����������� ��������������.*/

#pragma once
#include "vkFFT.h"
#include "utils_VkFFT.h"
#include <chrono>
using namespace std;
using namespace chrono;

/** ������������ ������������������ �������� �����, ��������� "����", ��������� �������� ������ � ����� �� GPU, ��������� �������������� ����� �� ������������� "�����", �������� ������ � GPU �� ����.
	@param vkGPU - ���������, ���������� ��������� Vulkan.
	@param isCompilerInitialized.
	@param arrLgth - ����� �������.
	@param loads - ���������� ��������.
	@param timeStart ����� - ������ �������.
	@return VKFFT_SUCCESS - � ������ ������, ��� ������ � ��������� ������.*/
VkFFTResult R2C_C2R(VkGPU* vkGPU, uint64_t isCompilerInitialized, bool file_output, FILE* output, uint64_t arrLgth,int loads, std::chrono::high_resolution_clock::time_point timeStart);
/** ������� ������.
	@param t - ������ �������.
	@return ������ �������*/
double SignalR(int t);
/** ������� ���.
	@param Signal - ������, ������������ �������� �������� ������� ����.
	@param Shum - ������ ������� ����.
	@return 0 � ������ ������*/
int CreateShum(std::vector<float>& Signal, std::vector<float>& Shum);